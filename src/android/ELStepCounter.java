/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.etos.stepcounter;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.widget.Toast;
import java.util.ArrayList;

/**
 *
 * @author catalin
 */
public class ELStepCounter implements SensorEventListener {

    private static final float STEP_THRESHOLD = 1.15f;
    private static final float NOISE_THRESHOLD = 2.75f;
    private static final float MAX_THRESHOLD = 1.5f;

    private static ELStepCounter sInstance;
    private ArrayList<Float> mEnergyValues = new ArrayList<Float>();
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private float mLastX;
    private float mLastY;
    private float mLastZ;
    private float mDeltaX;
    private float mDeltaY;
    private float mDeltaZ;
    private long mNumberOfSteps;
    private Context mContext;
    private StepCounterInterface mStepDetectionListener;

    private Preferences mPreferances;
    public long getmNumberOfSteps() {
        return mNumberOfSteps;
    }

    public StepCounterInterface getmStepDetectionListener() {
        return mStepDetectionListener;
    }

    public void setmStepDetectionListener(StepCounterInterface mStepDetectionListener) {
        this.mStepDetectionListener = mStepDetectionListener;
    }

    public static ELStepCounter getsInstance() {
        return sInstance;
    }

    public static void startInstance(Context context) {
        if (sInstance == null) {
            sInstance = new ELStepCounter();
            sInstance.mContext = context;
            sInstance.mPreferances = new Preferences(context);
            sInstance.initializeAccelerometer(); 
        }
    }

    private void initializeAccelerometer() {
        mEnergyValues = new ArrayList<Float>();
        mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        }

    }

    public void register() {
        if (mSensorManager != null && mAccelerometer != null) {
            mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        }
    }

    public void unregister() {
        if (mSensorManager != null && mAccelerometer != null) {
            mSensorManager.unregisterListener(this);
        }
    }

    public void onSensorChanged(SensorEvent event) {
        mDeltaX = Math.abs(mLastX - event.values[0]);
        mDeltaY = Math.abs(mLastY - event.values[1]);
        mDeltaZ = Math.abs(mLastZ - event.values[2]);
        
        if(mDeltaX < NOISE_THRESHOLD){
            mDeltaX = 0;
        }
        if(mDeltaY < NOISE_THRESHOLD){
            mDeltaY = 0;
        }
        if(mDeltaZ < NOISE_THRESHOLD){
            mDeltaZ = 0;
        }
        
        float energy = module(mDeltaX, mDeltaY, mDeltaZ);
        
        mEnergyValues.add(energy);
        if(mEnergyValues.size() == 3){
            float currentValue = mEnergyValues.get(1);
            if(isPeak(1) && (currentValue >= STEP_THRESHOLD) && (currentValue <= MAX_THRESHOLD)){
                mNumberOfSteps++;
                mPreferances.saveSteps(mNumberOfSteps);
                mStepDetectionListener.onStepDetected(mNumberOfSteps);
            }
            mEnergyValues.remove(0);
        }
    }

    private float module(float x, float y, float z){
        return (float) Math.sqrt(x*x + y*y + z*z) / 9.8f;
    }

    private boolean isPeak(int position){
        if(mEnergyValues.size() <= position + 1){
            return false;
        }
        float previous = mEnergyValues.get(position - 1);
        float next = mEnergyValues.get(position + 1);
        float current = mEnergyValues.get(position);
        return (current > previous) && (current > next);
    }
    public void onAccuracyChanged(Sensor arg0, int arg1) {

    }

}
