/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.etos.stepcounter;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.MultiSelectListPreference;

/**
 *
 * @author catalin
 */
public class Preferences {
    private static String PREFERANCES_NAME = "STEPCOUNTER";
    private static String STEPS = "Steps";
    private SharedPreferences mPreferances;
    private SharedPreferences.Editor mEditor;
    public Preferences(Context context){
        this.mPreferances = context.getSharedPreferences(PREFERANCES_NAME, Context.MODE_PRIVATE);
        this.mEditor = this.mPreferances.edit();
    }
    
    public long getSteps(){
        return this.mPreferances.getLong(STEPS, 0);
    }
    
    public void saveSteps(long steps){
        this.mEditor.putLong(STEPS, steps);
        this.mEditor.commit();
    }
    
    
}
