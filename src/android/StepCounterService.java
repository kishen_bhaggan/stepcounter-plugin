package nl.etos.stepcounter;

import nl.etos.loyaty.CordovaApp;
import nl.etos.loyaty.R;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.os.Process;
import android.widget.Toast;

public class StepCounterService extends Service {

	protected static final long SCREEN_OFF_RECEIVER_DELAY = 500;

	public StepCounterService(String name) {
		super();
		// TODO Auto-generated constructor stub
	}

	private WakeLock mWakeLock;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		ELStepCounter.startInstance(StepCounterService.this);
		mStepCounter = ELStepCounter.getsInstance();
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
				"StepCounterService");
		registerReceiver(mReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));
	}

	public BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.i(TAG, "onReceive(" + intent + ")");

			if (!intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
				return;
			}

			Runnable runnable = new Runnable() {
				public void run() {
					Log.i(TAG, "Runnable executing.");
					mStepCounter.unregister();
					mStepCounter.register();
				}
			};

			new Handler().postDelayed(runnable, SCREEN_OFF_RECEIVER_DELAY);
		}
	};

	public StepCounterService() {
		super();
	}

	private ELStepCounter mStepCounter;

	public static final String TAG = "STEP_COUNTER_SERVICE";

	private void publishResult(int steps) {
		Intent intent = new Intent(TAG);
		intent.putExtra("Steps", steps);
		sendBroadcast(intent);
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// TODO Auto-generated method stub
		super.onStartCommand(intent, flags, startId);
		//Notification notification = new Notification.Builder(this).build();
		Intent notifIntent = new Intent(this, CordovaApp.class);
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, notifIntent, Intent.FLAG_ACTIVITY_NEW_TASK);
		Notification notification = new NotificationCompat.Builder(this).setSmallIcon(R.drawable.icon).setContentTitle("Etos Loyalty").setOngoing(true).setContentInfo("Stepcounter is running").setContentIntent(pIntent).build();
		startForeground(1, notification);
		mWakeLock.acquire();
		mStepCounter.setmStepDetectionListener(new StepCounterInterface() {

			@Override
			public void onStepDetected(final long steps) {
				// TODO Auto-generated method stub
				//Toast.makeText(StepCounterService.this, steps + " steps",
				//		Toast.LENGTH_SHORT).show();
				publishResult((int) steps);

			}

			@Override
			public void error(int errorCode) {
				// TODO Auto-generated method stub

			}
		});
		return Service.START_STICKY;
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		unregisterReceiver(mReceiver);
		mStepCounter.unregister();
		stopForeground(true);
		mWakeLock.release();
		super.onDestroy();
	}
}
