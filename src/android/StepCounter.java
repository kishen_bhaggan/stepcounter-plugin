/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.etos.stepcounter;

import android.util.Log;
import android.widget.Toast;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;

/**
 *
 * @author catalin
 */
public class StepCounter extends CordovaPlugin {

    private ELStepCounter mStepCounter;
    private Preferences mPreferences;

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        mPreferences = new Preferences(cordova.getActivity());
        ELStepCounter.startInstance(cordova.getActivity());
        mStepCounter = ELStepCounter.getsInstance();
    }

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) {
        if (action.equals("startStepCounterUpdates")) {
            Toast.makeText(cordova.getActivity(), "Text", Toast.LENGTH_LONG).show();
            mStepCounter.setmStepDetectionListener(new StepCounterInterface() {

                public void onStepDetected(long steps) {

                    Toast.makeText(cordova.getActivity(), steps + " steps", Toast.LENGTH_SHORT).show();
                    PluginResult result = new PluginResult(PluginResult.Status.OK, (int) mPreferences.getSteps());
                    result.setKeepCallback(true);
                    callbackContext.sendPluginResult(result);
                }

                public void error(int errorCode) {

                }
            });

            return true;
        }

        return false;
    }

}
