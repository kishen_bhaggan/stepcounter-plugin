//
//  StepCounterDataStore.h
//  EtosLoyalty
//
//  Created by Kishen Bhaggan on 01/04/15.
//
//

#ifndef EtosLoyalty_StepCounterDataStore_h
#define EtosLoyalty_StepCounterDataStore_h

@interface StepCounterDataStore : NSObject 

- (void) saveStepCount:(int)stepCount;

- (NSArray*) getStepCountHistory: (NSDate*)startDate endDate:(NSDate*)nibEndDate;

- (int) getTotalStepCountForToday;

- (int) getAbsoluteTotal;

@end

#endif
