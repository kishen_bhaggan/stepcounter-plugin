//
//  Pedometer.m
//  EtosLoyalty
//
//  Created by Kishen Bhaggan on 19/02/15.
//
//

#import <Foundation/Foundation.h>
#import "Cordova/CDV.h"
#import "Cordova/CDVViewController.h"
#import "CoreMotion/CoreMotion.h"
#import "StepCounter.h"
#import "StepCounterDataStore.h"

@interface StepCounter()

@property (nonatomic) int initialStepCount;

@property (nonatomic,strong) CMStepCounter* stepCounter;

@property (nonatomic) BOOL usingStepCounter;

@property (nonatomic, strong) CMPedometer* pedometer;

@property (nonatomic) BOOL usingPedometer;

@property (readwrite, strong) NSString* callbackId;

@property (readwrite, strong) StepCounterDataStore* dataStore;

@property (nonatomic) BOOL usingAccelerometer;

@property (readwrite, strong) CMMotionManager* montionManager;

@end

@implementation StepCounter

#define NOISE_TRESHOLD 2.75
#define MAX_THRESHOLD 1.80
#define STEP_THRESHOLD 1.25

@synthesize callbackId;

- (void)pluginInitialize
{
    NSLog(@"Initializing plugin");
    self.dataStore = [[StepCounterDataStore alloc] init];
    self.initialStepCount = [self.dataStore getAbsoluteTotal];
    NSLog(@"Initial stepcount %d", self.initialStepCount);
}

 - (void) startStepCounterUpdates:(CDVInvokedUrlCommand *)command
{
    self.callbackId = command.callbackId;
    
    [self.commandDelegate runInBackground:^{
        BOOL isStepCounterAvailable = [CMStepCounter isStepCountingAvailable];
        BOOL isIosPedomterAvailable = [CMPedometer isStepCountingAvailable];
        
        if (isIosPedomterAvailable){
            self.usingPedometer = YES;
            self.pedometer = [[CMPedometer alloc] init];
            [self startStepCountingWithIOSPedometer];
        }
        else if (isStepCounterAvailable){
            self.usingStepCounter = YES;
            self.stepCounter = [[CMStepCounter alloc] init];
            [self startStepCountingWithIOSStepCounter];
        }
        else
        {
            CMMotionManager *manager = [[CMMotionManager alloc] init];
            BOOL isMotionManagerAvailable = [manager isAccelerometerAvailable];
            
            if (isMotionManagerAvailable)
            {
                self.montionManager = manager;
                [self startStepCountingWithAccelerometer];
            }
        }
    }];
}

- (void) startStepCountingWithIOSStepCounter
{
    NSLog(@"Starting step count with iOS Step Counter (iOS 7 and iPhone 5s)");
    __block CDVPluginResult* result = nil;
    [self.stepCounter startStepCountingUpdatesToQueue:[NSOperationQueue mainQueue]updateOn: 1 withHandler:^(NSInteger numberOfSteps, NSDate *timestamp, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error){
                result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[error localizedDescription]];
            }
            else{
                int stepCount = self.initialStepCount + numberOfSteps;
                [self.dataStore saveStepCount:stepCount];
                int stepCountForToday = [self.dataStore getTotalStepCountForToday];
                
                NSDictionary* stepCountResult = [[NSMutableDictionary alloc] init];
                [stepCountResult setValue:[NSNumber numberWithInt:stepCountForToday] forKey:@"today"];
                [stepCountResult setValue:[NSNumber numberWithInt:stepCount] forKey:@"total"];
                
                result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:stepCountResult];
                [result setKeepCallbackAsBool:true];
            }
            
            [self.commandDelegate sendPluginResult:result callbackId:self.callbackId];
        });
    }];
}

- (void) startStepCountingWithIOSPedometer
{
    NSLog(@"Starting step count with iOS built-in pedometer (iOS 8 and iPhone 5s+)");
    __block CDVPluginResult* result = nil;
    
    [self.pedometer startPedometerUpdatesFromDate:[NSDate date] withHandler:^(CMPedometerData *pedometerData, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error){
                result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:[error localizedDescription]];
            }
            else{
                int stepCount = self.initialStepCount + [pedometerData.numberOfSteps intValue];
                [self.dataStore saveStepCount:stepCount];
                int stepCountForToday = [self.dataStore getTotalStepCountForToday];
                
                NSDictionary* stepCountResult = [[NSMutableDictionary alloc] init];
                [stepCountResult setValue:[NSNumber numberWithInt:stepCountForToday] forKey:@"today"];
                [stepCountResult setValue:[NSNumber numberWithInt:stepCount] forKey:@"total"];
                
                result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:stepCountResult];
                [result setKeepCallbackAsBool:true];
            }
            [self.commandDelegate sendPluginResult:result callbackId:self.callbackId];
        });
    }];
}

- (void) startStepCountingWithAccelerometer
{
    NSLog(@"Starting step count accelerometer data (devices without built-in pedometer)");
    __block CDVPluginResult* result = nil;
    
    [self.montionManager setAccelerometerUpdateInterval:0.080];
    
    double lastX = 0.0;
    double lastY = 0.0;
    double lastZ = 0.0;
    
    __block int stepCount = self.initialStepCount;
    NSMutableArray* energyValues = [[NSMutableArray alloc] init];
    [self.montionManager startAccelerometerUpdatesToQueue:[NSOperationQueue mainQueue] withHandler:^(CMAccelerometerData *accelerometerData, NSError *error) {
        if (!error){
            double deltaX = fabs(lastX - accelerometerData.acceleration.x) * 9.8;
            double deltaY = fabs(lastY - accelerometerData.acceleration.y) * 9.8;
            double deltaZ = fabs(lastZ - accelerometerData.acceleration.z) * 9.8;
            
            if (deltaX < NOISE_TRESHOLD){
                deltaX = 0;
            }
            
            if (deltaY < NOISE_TRESHOLD){
                deltaY = 0;
            }
            
            if (deltaZ < 2){
                deltaZ = 0;
            }
            
            double energy = sqrt(deltaX* deltaX + deltaY*deltaY + deltaZ * deltaZ) / 9.8;
            NSNumber* number = [NSNumber numberWithDouble:energy];
            
            [energyValues addObject:number];
            
            if (energyValues.count == 3){
                double middleEnergy = [[energyValues objectAtIndex:1] doubleValue];
                
                if ([self isPeak:1 numberArray:energyValues] && middleEnergy > STEP_THRESHOLD && middleEnergy < MAX_THRESHOLD){                    stepCount = stepCount + 1;
                    [self.dataStore saveStepCount:stepCount];
                    int stepCountForToday = [self.dataStore getTotalStepCountForToday];
                    
                    NSDictionary* stepCountResult = [[NSMutableDictionary alloc] init];
                    [stepCountResult setValue:[NSNumber numberWithInt:stepCountForToday] forKey:@"today"];
                    [stepCountResult setValue:[NSNumber numberWithInt:stepCount] forKey:@"total"];
                    
                    result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:stepCountResult];
                    [result setKeepCallbackAsBool:true];
                    [self.commandDelegate sendPluginResult:result callbackId:self.callbackId];
                }
                
                [energyValues removeObjectAtIndex:0];
            }
        }
    }];
}

- (BOOL) isPeak:(int )index numberArray:(NSMutableArray *)array{
    BOOL result = NO;
    
    if (array.count > index + 1){
        double previous = [[array objectAtIndex:index-1] doubleValue];
        double current = [[array objectAtIndex:index] doubleValue];
        double next = [[array objectAtIndex:index+1] doubleValue];
        result = current > previous && current > next;
    }
    
    return result;
}

- (void) getStepCounterHistory:(CDVInvokedUrlCommand *)command
{
    NSLog(@"Getting history");
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate* startDate = [formatter dateFromString:[command argumentAtIndex:0]];
    NSDate* endDate = [formatter dateFromString:[command argumentAtIndex:1]];
    
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:[self.dataStore getStepCountHistory:startDate endDate:endDate]];
    
    [self.commandDelegate sendPluginResult:result callbackId:command.callbackId];
}

- (void) stopStepCounterUpdates:(CDVInvokedUrlCommand *)command
{
    if (self.usingPedometer){
        [self.pedometer stopPedometerUpdates];
    }
    else if (self.usingStepCounter){
        [self.stepCounter stopStepCountingUpdates];
    }
    else if (self.usingAccelerometer){
        [self.montionManager stopAccelerometerUpdates];
    }
}

@end