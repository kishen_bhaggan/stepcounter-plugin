//
//  StepCounterDataStore.m
//  EtosLoyalty
//
//  Created by Kishen Bhaggan on 01/04/15.
//
//

#import <Foundation/Foundation.h>
#import "StepCounterDataStore.h"

@implementation StepCounterDataStore

- (void) saveStepCount:(int)stepCount
{
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    NSDate* date = [NSDate date];
    NSMutableDictionary* todayData = [self getTodaysDictionary];
    
    NSDateFormatter* hourFormat = [[NSDateFormatter alloc] init];
    [hourFormat setDateFormat:@"HH:00"];
    NSString* hourKey = [hourFormat stringFromDate:date];
    
    NSDictionary* hourData = [[todayData valueForKey:hourKey] mutableCopy];
    
    if (hourData == nil){
        hourData = [[NSMutableDictionary alloc] init];
    }
    
    int totalForToday = 0;
    int absoluteTotal = 0;
    int totalOfCurrentHour = 0;
    
    NSNumber* totalCurrentHourFromDict = [hourData objectForKey:@"stepCount"];
    
    if (totalCurrentHourFromDict != nil){
        totalOfCurrentHour = [totalCurrentHourFromDict intValue];
    }
    
    NSNumber* absoluteTotalFromPrefs = [prefs objectForKey:@"absoluteTotal"];
    
    if (absoluteTotalFromPrefs != nil){
        absoluteTotal = [absoluteTotalFromPrefs intValue];
    }
    
    NSNumber* totalForTodayValue = [todayData objectForKey:@"total"];
    
    if (totalForTodayValue != nil){
        totalForToday = [totalForTodayValue intValue];
    }
    
    int delta = stepCount - absoluteTotal;
    totalForToday += delta;
    totalOfCurrentHour += delta;
    absoluteTotal = stepCount;
    NSLog(@"Currenthour %d", totalOfCurrentHour);
    [hourData setValue:[NSNumber numberWithInt:totalOfCurrentHour] forKey:@"stepCount"];
    [todayData setValue:hourData forKey:hourKey];
    [todayData setValue:[NSNumber numberWithInt:totalForToday] forKey:@"total"];
    
    NSMutableDictionary* data = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"stepCounterData"] mutableCopy];
    if (data == Nil){
        data = [[NSMutableDictionary alloc] init];
    }
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString* dateString = [formatter stringFromDate:date];

    [data setValue:todayData forKey:dateString];
    [prefs setObject:data forKey:@"stepCounterData"];
    [prefs setValue:[NSNumber numberWithInt:absoluteTotal] forKey:@"absoluteTotal"];
    [prefs synchronize];
}

- (NSArray*) getStepCountHistory:(NSDate *)startDate endDate:(NSDate *)nibEndDate
{
    NSMutableArray* result = [[NSMutableArray alloc] init];
    for(NSDate* currentDate = startDate; [currentDate compare:nibEndDate] == NSOrderedAscending; currentDate = [currentDate dateByAddingTimeInterval:3600*24])
    {
        NSDictionary* currentDateData = [self getDayDataDictionary:currentDate];
        
        NSMutableDictionary* dateInfo = [[NSMutableDictionary alloc] init];
        NSNumber* total = [currentDateData valueForKey:@"total"];
        
        if (total == nil){
            total = [NSNumber numberWithInt:0];
        }
        
        [dateInfo setObject:total forKey:@"total"];
        
        NSMutableArray* dateHourData = [[NSMutableArray alloc] init];
        
        for(int hour = 0; hour < 24; hour++){
            NSMutableDictionary* hourData = [[NSMutableDictionary alloc] init];
            NSDictionary* savedDate = [self getDataForTheHour:hour dictionary:currentDateData];
            NSString* key = [NSString stringWithFormat:@"%02d:00:00", hour];
            NSLog(@"Hourkey: %@", key);
            if (savedDate != nil){
                [hourData setValue:[savedDate valueForKey:@"stepCount"] forKey:key];
            }
            else{
                [hourData setValue:[NSNumber numberWithInt:0] forKey:key];
            }
            [dateHourData addObject:hourData];
        }
        
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        
        NSString* dateString = [formatter stringFromDate:currentDate];
        
        [dateInfo setObject:dateHourData forKey:dateString];
        [result addObject:dateInfo];
    }
    
    return result;
}

- (NSDictionary*) getDataForTheHour:(NSInteger) hour dictionary:(NSDictionary*) dateData
{
    NSString* key = [NSString stringWithFormat:@"%0ld:00", (long)hour];
    return [dateData objectForKey:key];
}

- (int) getTotalStepCountForToday
{
    NSDictionary* todayData = [self getTodaysDictionary];
    int result = 0;
    NSNumber* total = [todayData objectForKey:@"total"];
    
    if (total != nil){
        result = [total intValue];
    }
    
    
    return result;
}

- (int) getAbsoluteTotal
{
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    NSNumber* absoluteTotalFromPrefs = [prefs objectForKey:@"absoluteTotal"];
    int absoluteTotal = 0;
    if (absoluteTotalFromPrefs != nil){
        absoluteTotal = [absoluteTotalFromPrefs intValue];
    }
    
    return absoluteTotal;
}

- (NSMutableDictionary*) getDayDataDictionary: (NSDate*) date
{
    NSUserDefaults* prefs = [NSUserDefaults standardUserDefaults];
    
    NSMutableDictionary* data = [[prefs dictionaryForKey:@"stepCounterData"] mutableCopy];
    
    NSMutableDictionary* prefData = nil;
    if (data == Nil){
        NSLog(@"dictionary not found");
        prefData = [[NSMutableDictionary alloc] init];
    }
    
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString* dateString = [formatter stringFromDate:date];
    NSLog(@"Number of items in data: %lu", (unsigned long)[data count]);
    NSMutableDictionary* todayData = [[data valueForKey:dateString] mutableCopy];
    
    if (todayData == nil){
        NSLog(@"Todays data not found: %@", dateString);
        todayData = [[NSMutableDictionary alloc] init];
    }
    
    return todayData;
}

- (NSMutableDictionary*) getTodaysDictionary
{
    return [self getDayDataDictionary:[NSDate date]];
}

@end