//
//  Pedometer.h
//  EtosLoyalty
//
//  Created by Kishen Bhaggan on 19/02/15.
//
//

#ifndef EtosLoyalty_Pedometer_h
#define EtosLoyalty_Pedometer_h

#import "Cordova/CDV.h"

@interface StepCounter : CDVPlugin

- (void) startStepCounterUpdates:(CDVInvokedUrlCommand*)command;

- (void) stopStepCounterUpdates:(CDVInvokedUrlCommand*)command;

- (void) getStepCounterHistory: (CDVInvokedUrlCommand*)command;

@end


#endif
